//
//  MCPlayerView.swift
//  DeadCodeMediaCenter
//
//  Created by Mac on 7/16/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class MCPlayerView: UIView {

    var media:MCMedia?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var playerHolderView: PlayerView!
    
    var playerItem:AVPlayerItem?
    var player = AVPlayer()
    @IBOutlet weak var playButton: UIButton!
    
    
    
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView? {
        return UINib(
            nibName: nibNamed,
            bundle: bundle
            ).instantiateWithOwner(nil, options: nil)[0] as? UIView
    }
    
    
    internal func getViewWithMedia(media:MCMedia) -> UIView {
        
        playButton.layer.borderWidth = 1.5
        playButton.layer.borderColor = UIColor.whiteColor().CGColor
        
        let mediaType:MediaType = media.fileType! as MediaType
        switch mediaType {
        case .image:
            self.imageView.backgroundColor = UIColor.lightGrayColor()
            self.imageView.image = UIImage(named: MCFileManager.getDocumentsDirectory().stringByAppendingString("/\((media.fileName)!)"))//stringsByAppendingPaths(media.fileName))

            self.imageView.hidden = false
            self.webView.hidden = true
            playButton.hidden = true
            self.playerHolderView.hidden = true


            break
        case .document:
            let url : NSURL! = NSURL(string: MCFileManager.getDocumentsDirectory().stringByAppendingString("/\((media.fileName)!)"))
            self.webView.backgroundColor = UIColor.lightGrayColor()
            self.webView.loadRequest(NSURLRequest(URL: url))
            self.imageView.hidden = true
            self.webView.hidden = false
            playButton.hidden = true
            self.playerHolderView.hidden = true

            break
        case .video,.audio:
            self.imageView.hidden = true
            self.webView.hidden = true
            playButton.hidden = false
            
            let url = NSURL(fileURLWithPath: MCFileManager.getDocumentsDirectory().stringByAppendingString("/\((media.fileName)!)") as String)
            playerItem = AVPlayerItem(URL: url)
            player = AVPlayer(playerItem: playerItem!)
            playerHolderView.playerLayer.player = player            
            break
        case .none:
            break
        }
       
        return self
    }
    
    
    @IBAction func playButtonAction(sender: UIButton) {
        player.play()
        playButton.hidden = true
       
    }

}
