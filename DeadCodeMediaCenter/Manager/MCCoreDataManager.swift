//
//  MCCoreDataManager.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 17/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class MCCoreDataManager: NSObject {
    
    /**
     * Inserting Playlist data in Database
     * @return: Void
     */
    func insertPlaylistIntoDB(playListName: String, medias: [MCMedia]) -> Void {
        
        let playList: CDPlayList = (NSEntityDescription.insertNewObjectForEntityForName("CDPlayList", inManagedObjectContext:appDelegate.managedObjectContext)) as! CDPlayList
        playList.medias = createCDMediaList(medias)
        playList.playListName = playListName
        
        do {
            try appDelegate.managedObjectContext.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }
        
    }
    
    /**
     * Creating MCMedia type to NSSet
     * @return: NSSet
     */
    func createCDMediaList(medias: [MCMedia]) -> NSSet {
        let cdMedias: NSMutableSet = NSMutableSet()

        for media in medias {
            print(media)
            let mediaMO: CDMedia = (NSEntityDescription.insertNewObjectForEntityForName("CDMedia", inManagedObjectContext:appDelegate.managedObjectContext)) as! CDMedia
            mediaMO.displayName = media.displayName
            mediaMO.fileExtension = media.fileExtension
            mediaMO.fileName = media.fileName
            mediaMO.fileType = media.fileType?.rawValue
            mediaMO.iconName = media.iconName
            mediaMO.isSelectedToPlay = NSNumber(bool: media.isSelectedToPlay)
            cdMedias.addObject(mediaMO)
            
        }
        
        return cdMedias
        
    }
    
    /**
     * Creating Fetching Data from Databse
     * @return: [CDPlayList]
     */
    func fetchAllPlayListFromDB() -> [CDPlayList] {
        var playLists: [CDPlayList] = []
        let fetchRequest = NSFetchRequest()
        let entityDescription = NSEntityDescription.entityForName("CDPlayList", inManagedObjectContext: appDelegate.managedObjectContext)
        fetchRequest.entity = entityDescription
        
        do {
            let  fetchPlayLists = try appDelegate.managedObjectContext.executeFetchRequest(fetchRequest) as! [CDPlayList]
            
            if fetchPlayLists.count > 0 {
                playLists = fetchPlayLists
            }
            
        } catch {
            let fetchError = error as NSError
            print(fetchError)
        }
        
        return playLists
    }
    
    /**
     * Creating Deleting Data from Databse
     * @return: Void
     */
    func deletePlaylistFromDB(aPlaylist: CDPlayList) -> Void {
        let playlist: CDPlayList = aPlaylist
        
        appDelegate.managedObjectContext.deleteObject(playlist)
        
        do {
            try appDelegate.managedObjectContext.save()
        } catch {
            fatalError("Failure to save context: \(error)")
        }

    }
    

    /**
     * [CDMedia] to [MCMedia]  converter
     * @return: [MCMedia]
     */
    func cdMediaToMcMediaModelConverter(coreDataList: [CDMedia]) -> [MCMedia] {
        
        var mediaList: [MCMedia] = []
        
        for cdMedia in coreDataList {
            let media = MCMedia.init(fileName: cdMedia.fileName!)
            mediaList.append(media)
            
        }
        return mediaList
    }
    
}
