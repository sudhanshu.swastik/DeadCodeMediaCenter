//
//  CDPlayList+CoreDataProperties.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 17/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension CDPlayList {

    @NSManaged var playListName: String?
    @NSManaged var medias: NSSet?

}
