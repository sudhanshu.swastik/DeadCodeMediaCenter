//
//  MCMedia.swift
//  DeadCodeMediaCenter
//
//  Created by Sudhanshu Shukla V on 16/07/16.
//  Copyright © 2016 Sudhanshu Shukla V. All rights reserved.
//

import UIKit



class MCMedia: NSObject {
    var fileName: String?
    var displayName: String?
    var fileType: MediaType?
    var fileExtension: String?
    var iconName: String?
    var isSelectedToPlay = false
    
    
    init(fileName: String) {
        
        self.fileName = fileName
        self.displayName = MCFileManager.displayNameExtractor(fileName)
        self.fileType = MCMedia.fileTypeFinder(fileName)
        self.fileExtension = MCFileManager.extentionExtractor(fileName)
        self.iconName = MCMedia.fileIconFinder(self.fileType!)
        self.isSelectedToPlay = false
    }
    
    class func fileTypeFinder(fileName: String) -> MediaType {
        
        let fileExtention = MCFileManager.extentionExtractor(fileName)
        
        if fileExtention == "pdf" || fileExtention == "doc" || fileExtention == "docx" || fileExtention == "ppt" || fileExtention == "pptx" {
            return MediaType.document
        }
        if fileExtention == "mp3" {
            return MediaType.audio
        }
        if fileExtention == "mp4" || fileExtention == "mov" || fileExtention == "wmv" || fileExtention == "m4v" {
            return MediaType.video
        }
        if fileExtention == "jpeg" || fileExtention == "jpg" || fileExtention == "png" {
            return MediaType.image
        }
        return MediaType.none
    }
    
    class func fileIconFinder(mediaType: MediaType) -> String {
        
        switch mediaType {
        case .audio:
            return "file-icon_Audio"
        case .video:
            return "file-icon_Video"
        case .document:
            return "file-icon_PDF"
        case .image:
            return "file-icon_Image"
        case .none:
            return ""
        }
    }
    
}
